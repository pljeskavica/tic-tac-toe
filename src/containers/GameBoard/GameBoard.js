import React from 'react';
import PropTypes from 'prop-types';

// HoC
import { withStyles } from '@material-ui/core/styles';

// Components
import GameRow from 'components/GameRow';

const styles = theme => ({
  root: {
    height: '600px',
    width: '620px',
    display: 'grid',
    background: theme.palette.primary.main,
    gap: '10px',
    gridTemplateColumns: '1fr',
    gridTemplateRows: '1fr 1fr 1fr',
    borderRadius: '10px',
  },
});

const GameBoard = ({ boardConfiguration = [], onClick, classes = {} } = {}) => (
  <div className={classes.root}>
    {boardConfiguration.map((row, index) => (
      <GameRow
        rowConfiguration={row}
        key={`${index}-${row.join('')}`}
        onClick={({ squareIndex }) => onClick({ rowIndex: index, squareIndex })}
      />
    ))}
  </div>
);

GameBoard.propTypes = {
  boardConfiguration: PropTypes.array.isRequired,
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(GameBoard);
