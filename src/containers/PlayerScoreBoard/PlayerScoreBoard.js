import React from 'react';
import PropTypes from 'prop-types';

// HoC
import { withStyles } from '@material-ui/core/styles';

// Components
import PlayerCard from 'components/PlayerCard';
import Typography from '@material-ui/core/Typography';

// Utilities
import classNames from 'class-names';

const styles = theme => ({
  root: {
    display: 'grid',
    height: '100%',
    width: '100%',
    gridTemplateColumns: '1fr 1fr',
    placeItems: 'center',
  },
  player1: {
    gridRow: '1',
    gridColumn: '1',
    backgroundColor: theme.palette.primary.main,
    color: theme.palette.primary.contrastText,
  },
  player2: {
    gridRow: '1',
    gridColumn: '2',
    backgroundColor: theme.palette.secondary.main,
    color: theme.palette.secondary.contrastText,
  },
  catsGame: {
    color: theme.palette.primary.contrastText,
    textDecoration: 'underline',
    gridRow: '1',
    gridColumn: '1 / -1',
    placeSelf: 'center',
  },
});

const PlayerScoreBoard = props => {
  const {
    playerNames,
    gameScore,
    gameActive,
    activePlayer,
    initialLoad,
    classes = {},
  } = props;

  const cardClass = index =>
    classNames({
      [classes.player1]: index === 0,
      [classes.player2]: index === 1,
    });

  const catsGame = !gameActive && activePlayer === null && !initialLoad;

  return (
    <div className={classes.root}>
      {playerNames.map((name, index) => (
        <PlayerCard
          key={name}
          name={name}
          score={gameScore[index]}
          classes={{
            root: cardClass(index),
          }}
          isActivePlayer={activePlayer === index}
          gameActive={gameActive}
        />
      ))}
      {catsGame && (
        <Typography className={classes.catsGame} variant="h5">
          Cats Game
        </Typography>
      )}
    </div>
  );
};

PlayerScoreBoard.propTypes = {
  playerNames: PropTypes.array.isRequired,
  gameScore: PropTypes.array.isRequired,
  activePlayer: PropTypes.number, // Could be null
};

export default withStyles(styles)(PlayerScoreBoard);
