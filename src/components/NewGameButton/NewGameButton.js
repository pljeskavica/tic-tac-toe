import React from 'react';
import PropTypes from 'prop-types';

// Components
import Fab from '@material-ui/core/Fab';
import AddIcon from '@material-ui/icons/Add';
import Tooltip from '@material-ui/core/Tooltip';

const NewGameButton = ({ onClick, disabled, classes = {} } = {}) => (
  <Tooltip title="New Game">
    <Fab
      color="primary"
      aria-label="New Game"
      className={classes.root}
      onClick={onClick}
      disabled={disabled}
    >
      <AddIcon />
    </Fab>
  </Tooltip>
);

NewGameButton.propTypes = {
  onClick: PropTypes.func.isRequired,
  disabled: PropTypes.bool.isRequired,
  classes: PropTypes.object.isRequired,
};

export default NewGameButton;
