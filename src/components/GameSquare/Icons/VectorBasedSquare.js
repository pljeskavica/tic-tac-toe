import React from 'react';

export default () => (
  <svg
    width="180"
    height="180"
    version="1.1"
    xmlns="http://www.w3.org/2000/svg"
  >
    <rect
      x="40"
      y="40"
      width="100"
      height="100"
      stroke="black"
      fill="transparent"
      strokeWidth="5"
    />
  </svg>
);
