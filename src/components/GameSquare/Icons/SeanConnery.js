import React from 'react';

export default () => (
  <img
    height="auto"
    width="150px"
    src="https://m.media-amazon.com/images/M/MV5BMjA5OTQ5NTY0M15BMl5BanBnXkFtZTgwNjUzNTU3MTE@._V1_SY1000_CR0,0,1248,1000_AL_.jpg"
    alt="SeanConnery Zardoz"
  />
);
