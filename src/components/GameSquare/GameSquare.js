import React from 'react';
import PropTypes from 'prop-types';

// HoC
import { withStyles } from '@material-ui/core/styles';

// Icons
import VectorBasedSquare from './Icons/VectorBasedSquare';
import SeanConnery from './Icons/SeanConnery';

const styles = theme => ({
  root: {
    height: '100%',
    width: '100%',
    background: '#f1f1f1',
    display: 'grid',
    placeItems: 'center',
    borderRadius: '10px',
  },
});

const GameSquare = ({
  squareConfiguration,
  disabled,
  onClick,
  classes = {},
}) => (
  <div className={classes.root} onClick={disabled ? null : onClick}>
    {squareConfiguration === null && null}
    {squareConfiguration === 'x' && <VectorBasedSquare />}
    {squareConfiguration === 'o' && <SeanConnery />}
  </div>
);

GameSquare.propTypes = {
  squareConfiguration: PropTypes.string, // Could be null
  disabled: PropTypes.bool.isRequired,
  onClick: PropTypes.func.isRequired,
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(GameSquare);
