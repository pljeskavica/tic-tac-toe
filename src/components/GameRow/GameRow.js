import React from 'react';
import PropTypes from 'prop-types';

// HoC
import { withStyles } from '@material-ui/core/styles';

// Components
import GameSquare from 'components/GameSquare';

const styles = theme => ({
  root: {
    height: '100%',
    width: '100%',
    display: 'grid',
    gridAutoFlow: 'column',
    boxSizing: 'border-box',
    gap: '10px',
    gridAutoColumns: '200px',
  },
});

const GameRow = ({ rowConfiguration = [], onClick, classes = {} }) => {
  return (
    <div className={classes.root}>
      {rowConfiguration.map((square, index) => (
        <GameSquare
          squareConfiguration={square}
          key={`${index}-${square}`}
          disabled={square !== null}
          onClick={() => onClick({ squareIndex: index })}
        />
      ))}
    </div>
  );
};

GameRow.propTypes = {
  rowConfiguration: PropTypes.array.isRequired,
  onClick: PropTypes.func.isRequired,
  classes: PropTypes.object.isRequired,
};
export default withStyles(styles)(GameRow);
