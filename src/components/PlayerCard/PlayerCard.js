import React from 'react';
import PropTypes from 'prop-types';

// HoC
import { withStyles } from '@material-ui/core/styles';

// Components
import Typography from '@material-ui/core/Typography';

const styles = theme => ({
  root: {
    height: '100%',
    width: '100%',
    display: 'grid',
    placeItems: 'center',
    gridTemplateRows: '1fr 1fr',
    gridTemplateColumns: '1fr 1fr 1fr',
  },
  playerName: {
    color: theme.palette.primary.contrastText,
    gridRow: '1 / -1',
    gridColumn: '1 / -1',
  },
  yourTurnMessage: {
    color: theme.palette.primary.contrastText,
    textDecoration: 'underline',
    transform: 'rotate(-30deg)',
    justifySelf: 'start',
    gridRow: '1 / -1',
    gridColumn: '1',
  },
});

const PlayerCard = ({
  name,
  score,
  gameActive,
  isActivePlayer,
  classes = {},
} = {}) => (
  <div className={classes.root}>
    {isActivePlayer && gameActive && (
      <Typography variant="h6" className={classes.yourTurnMessage}>
        Your Turn!
      </Typography>
    )}
    {isActivePlayer && !gameActive && (
      <Typography variant="h6" className={classes.yourTurnMessage}>
        Winner!
      </Typography>
    )}
    <Typography variant="h4" className={classes.playerName}>
      {name} - Score: {score}
    </Typography>
  </div>
);

PlayerCard.propTypes = {
  name: PropTypes.string.isRequired,
  score: PropTypes.number.isRequired,
  gameActive: PropTypes.bool.isRequired,
  isActivePlayer: PropTypes.bool.isRequired,
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(PlayerCard);
