// React
import React from 'react';
import ReactDOM from 'react-dom';

// NPM Providers
import { MuiThemeProvider } from '@material-ui/core/styles';

// Constants
import theme from 'shared/theme';

// Utilities
import * as serviceWorker from './serviceWorker';

// Css
import './index.css';

// Component
import App from './App';

const render = Component => {
  ReactDOM.render(
    <MuiThemeProvider theme={theme}>
      <Component />
    </MuiThemeProvider>,
    document.getElementById('root'),
  );
};

// Turn off service worker
serviceWorker.unregister();

render(App);
