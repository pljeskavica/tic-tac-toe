export default boardConfiguration => {
  // // Check for horizontal wins
  for (let i = 0; i < boardConfiguration.length; i += 1) {
    const testingSquare = boardConfiguration[i][0];
    if (testingSquare === null) break;
    const horizontalWin = boardConfiguration[i].every(
      square => square === testingSquare,
    );
    if (horizontalWin) return testingSquare;
  }

  // Check for vertical win
  for (let i = 0; i < boardConfiguration[0].length; i += 1) {
    const testingSquare = boardConfiguration[0][i];
    if (testingSquare === null) break;

    const verticalWin = boardConfiguration.every(
      row => row[i] === testingSquare,
    );
    if (verticalWin) return testingSquare;
  }

  // Check for Diagonal Win
  // Top left  => bottom right
  if (
    boardConfiguration[0][0] !== null &&
    boardConfiguration[0][0] === boardConfiguration[1][1] &&
    boardConfiguration[0][0] === boardConfiguration[2][2]
  ) {
    return boardConfiguration[0][0];
  }
  // Top right => bottom left
  if (
    boardConfiguration[0][2] !== null &&
    boardConfiguration[0][2] === boardConfiguration[1][1] &&
    boardConfiguration[0][2] === boardConfiguration[2][0]
  ) {
    return boardConfiguration[0][2];
  }
  // Check for Cats Game
  return boardConfiguration.every(row => row.every(square => square !== null));
};
