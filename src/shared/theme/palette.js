export default {
  primary: {
    main: '#6200EE',
    dark: '#3700B3',
    contrastText: '#FFFFFF',
  },
  secondary: {
    main: '#03DAC5',
    contrastText: '#FFFFFF',
  },
  error: {
    main: '#B00020',
    contrastText: '#FFFFFF',
  },
};
