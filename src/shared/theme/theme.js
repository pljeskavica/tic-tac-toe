import { createMuiTheme } from '@material-ui/core/styles';
// Custom Theme
import palette from './palette';

const theme = createMuiTheme({
  palette,
});

export default theme;
