import React, { useState, useEffect } from 'react';
import PropTypes from 'prop-types';

import { withStyles } from '@material-ui/core/styles';

// Containers
import PlayerScoreBoard from 'containers/PlayerScoreBoard';
import GameBoard from 'containers/GameBoard';

// Components
import NewGameButton from 'components/NewGameButton';

// Utilities
import isGameOver from 'utilities/isGameOver';

const styles = theme => ({
  root: {
    display: 'grid',
    placeItems: 'center',
    gridTemplateColumns: '1fr 1fr 1fr',
    gridTemplateRows: '100px 3fr 1fr',
    gap: '30px',
  },
  playerScoreBoard: {
    gridRow: '1',
    gridColumn: '1 / -1',
  },
  gameBoard: {
    gridRow: '2',
    gridColumn: '1 / -1',
  },
  newGameButton: {
    gridRow: '3',
    gridColumn: '-2',
  },
});

const App = props => {
  const { classes = {} } = props;

  const blankBoardConfiguration = () => {
    const boardRow = Array(3).fill(null);
    return [boardRow, boardRow, boardRow];
  };
  const [initialLoad, setInitialLoad] = useState(true);
  const [gameActive, setGameActive] = useState(false);
  const [gameScore, setGameScore] = useState([0, 0]);
  const [playerNames] = useState(['Player 1', 'Player 2']); // Future plans to allow users to modify name
  const [playerXorO, setPlayerXorO] = useState([null, null]);
  const [activePlayer, setActivePlayer] = useState(null);
  const [boardConfiguration, setBoardConfiguration] = useState(
    blankBoardConfiguration(),
  );

  useEffect(() => {}, [boardConfiguration]);

  const newGameOnClick = () => {
    // Dont alow users to start a new game if the game is active
    if (gameActive) return;
    setInitialLoad(false); // Helps track cats game message
    setGameActive(true); // Start the game
    setActivePlayer(0); // Set the first player to active player
    setPlayerXorO(['x', 'o']); // Set the player choice values
    setBoardConfiguration(blankBoardConfiguration()); // Clear the board
  };

  const selectSquare = ({ rowIndex, squareIndex }) => {
    // Dont allow users to click on squares when the game is not active
    if (!gameActive) return;
    // Find the value of the new square based on the activePlayer index
    const squareValue = playerXorO[activePlayer];
    // Copy the row selected
    const newRowConfiguration = [...boardConfiguration[rowIndex]];
    // Insert the new square value
    newRowConfiguration.splice(squareIndex, 1, squareValue);
    // Copy the board configuration
    const newBoardConfiguration = [...boardConfiguration];
    // Insert the new row into the board configuration
    newBoardConfiguration.splice(rowIndex, 1, newRowConfiguration);
    // Update the state with the new board config
    setBoardConfiguration(newBoardConfiguration);
    // Check if the game is over
    const gameOver = isGameOver(newBoardConfiguration);
    // If the game is over with no winner
    // Its a cats game
    if (gameOver === true) {
      setGameActive(false);
      setActivePlayer(null);
      // If the game is over
      // Then we know that the active player is the winner
    } else if (gameOver) {
      setGameActive(false);
      // Copy the game score
      const newGameScore = [...gameScore];
      // Insert the new score based on the active player index
      newGameScore.splice(activePlayer, 1, gameScore[activePlayer] + 1);
      setGameScore(newGameScore);
      // The game is not over, switch active players
    } else {
      setActivePlayer(activePlayer === 0 ? 1 : 0);
    }
  };

  return (
    <div className={classes.root}>
      <PlayerScoreBoard
        playerNames={playerNames}
        gameScore={gameScore}
        activePlayer={activePlayer}
        classes={{
          root: classes.playerScoreBoard,
        }}
        gameActive={gameActive}
        initialLoad={initialLoad}
      />
      <GameBoard
        boardConfiguration={boardConfiguration}
        onClick={selectSquare}
        classes={{
          root: classes.gameBoard,
        }}
      />
      <NewGameButton
        disabled={gameActive}
        onClick={newGameOnClick}
        classes={{
          root: classes.newGameButton,
        }}
      />
    </div>
  );
};

App.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(App);
